<%--
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<tr>
    <td id="total">Total
        <c:if test="${activeLabel != null}"> for
            <span class="aLabel" onclick="clearFilter()">${activeLabel}</span>
        </c:if>
        <span id="forLabelsString"> = <span id="totalValue"><fmt:formatNumber value="${total}" type="currency" currencySymbol="€"/></span></span>
    </td>
</tr>