<%--
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>KaChing Personal Finances Manager</title>
    <meta name="author" content="Teo Mrnjavac"/>
    <meta name="keywords" content="money,finances,manager"/>
    <meta name="description" content="A simple personal finances manager."/>

    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.17.custom.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" type="text/css" href="css/form.css"/>

    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
    <div class="header">
        <img src="images/java.png" alt=""/>
        <span id="appname"><span id="ka">Ka</span>Ching
            <span id="pfm">Personal Finances Manager</span>
        </span>
        <span id="username">
            ${user.nickname} &lt;${user.email}&gt; <a href=${logoutUrl}>Sign out</a>
        </span>
    </div>
    ${status}
    <div id="sidebar">

        <div id="labelslist">
            <div class="caption">My labels</div>
            <table id="mylabels">
                <thead></thead>
                <tbody id="updatable-labels">
                    <jsp:include page="labels-rows.jsp"/>
                </tbody>
            </table>
        </div>
    </div>
    <div class="midpage">
        <div class="caption">My expenses
            <table id="totalTable">
                <tbody id="updatable-total">
                <jsp:include page="total.jsp"/>
                </tbody>
            </table>
        </div>
        <div id="toolbar">
            <div class="myButton" id="addExpenseButton" onclick="openAddExpenseFrame()">Add expense</div>
            <div class="myButton" id="clearFilterButton" onclick="clearFilter()">Show all</div>
        </div>
        <div id="addEntryArea">
            <fieldset>
                <legend>New expense entry</legend>
                <form id="addEntryForm" action="/app/submitEntry" method="POST">
                    <p>
                        <script type="text/javascript">
                            $(function()
                            {
                                $('#labelEdit').autocomplete(
                                        {
                                            source: "/app/labelcompleter",
                                            minLength: 0,
                                            autoFocus: false
                                        }
                                ).focus( function()
                                        {
                                            $(this).data("autocomplete").search($(this).val());
                                        }
                                );
                            });
                        </script>
                        <span class="ui-widget">
                            <label for="labelEdit">Label</label>
                            <input id="labelEdit" type='text' name="label" />
                            <img src="images/gear.png" alt="" width="16" height="16"/>
                        </span>
                    </p>
                    <p>
                        <label for="amountEdit">Amount</label>
                        <input id="amountEdit" type='text' name="amount"/> €
                    </p>
                    <p>
                        <script>
                            $(function() {
                                $( "#dateEdit" ).datepicker({ dateFormat: 'dd/mm/yy' });
                            });
                        </script>
                        <label for="dateEdit">Date</label>
                        <input id="dateEdit" type='text' name="date"/>
                        <input id="idEdit" type='text' name="id" hidden/>
                    </p>
                    <p>
                        <label for="descriptionEdit">Description</label>
                        <textarea id="descriptionEdit" name="description"></textarea>
                    </p>
                </form>
                <span id="buttons">
                    <span class="myButton" id="doneAddExpenseButton" onclick="doneAddExpenseFrame()">Add</span>
                    <span class="myButton" id="doneModifyExpenseButton" onclick="doneModifyExpenseFrame()">Apply changes</span>
                    <span class="myButton" id="doneDeleteExpenseButton" onclick="doneDeleteExpenseFrame()">Delete</span>
                    <span class="myButton" id="cancelAddExpenseButton" onclick="cancelAddExpenseFrame()">Cancel</span>
                </span>
            </fieldset>
        </div>

        <table id="expensesTable">
            <thead>
                <tr>
                    <th>Label</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody id="updatable-entries">
                <jsp:include page="expense-rows.jsp"/>
            </tbody>
        </table>
    </div>
    <div class="footer"></div>

</body>
</html>