/**
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
 */

openAddExpenseFrame = function()
{
    toggleAddExpenseFrame();
    $("#addEntryArea legend").replaceWith( '<legend>Add expense entry</legend>' );
    $("#doneAddExpenseButton").css( 'display', 'inline-block' );
    $("#doneModifyExpenseButton").css( 'display', 'none' );
    $("#doneDeleteExpenseButton").css( 'display', 'none' );
}

openModifyExpenseFrame = function( id, label, desc, date, amount)
{
    var addExpenseFrame = document.getElementById("addEntryArea");
    var toolbar = document.getElementById("toolbar");

    addExpenseFrame.style.display = 'block';
    toolbar.style.display = 'none';

    $("#addEntryArea legend").replaceWith('<legend>Modify or delete expense entry</legend>');
    $("#doneAddExpenseButton").css( 'display', 'none' );
    $("#doneModifyExpenseButton").css( 'display', 'inline-block' );
    $("#doneDeleteExpenseButton").css( 'display', 'inline-block' );

    $("#labelEdit").val( label );
    $("#amountEdit").val(amount);
    $("#descriptionEdit").val( desc );
    $("#dateEdit").val(date);
    $("#idEdit").val(id);
}

cancelAddExpenseFrame = function()
{
    var form = document.getElementById( "addEntryForm" )
    form.reset();
    toggleAddExpenseFrame();
}

doneAddExpenseFrame = function()
{
    $.ajax(
        {
            url: "/app/submitEntry",
            type: "POST",
            data: "label=" + $("#labelEdit").val()+
                "&amount="+ $("#amountEdit").val()+
                "&description="+ $("#descriptionEdit").val()+
                "&date="+ $("#dateEdit").val(),
            success: function(parsedResponse)
            {
                $("#updatable-entries").load("?partial-entries");
                $("#updatable-labels").load("?partial-labels");
                $("#updatable-total").load("?partial-total");

                $("#clearFilterButton").css( 'display', 'none' );
            }
        }
    );
    var form = document.getElementById( "addEntryForm" )
    form.reset();
    toggleAddExpenseFrame();
}

doneModifyExpenseFrame = function()
{
    $.ajax(
        {
            url: "/app/modifyEntry",
            type: "POST",
            data: "label=" + $("#labelEdit").val()+
                "&amount="+ $("#amountEdit").val()+
                "&description="+ $("#descriptionEdit").val()+
                "&date="+ $("#dateEdit").val()+
                "&id="+ $("#idEdit").val(),
            success: function(parsedResponse)
            {
                $("#updatable-entries").load("?partial-entries");
                $("#updatable-labels").load("?partial-labels");
                $("#updatable-total").load("?partial-total");

                $("#clearFilterButton").css( 'display', 'none' );
            }
        }
    );
    var form = document.getElementById( "addEntryForm" )
    form.reset();
    toggleAddExpenseFrame();
}

doneDeleteExpenseFrame = function()
{
    $.ajax(
        {
            url: "/app/deleteEntry",
            type: "POST",
            data: "id="+ $("#idEdit").val(),
            success: function(parsedResponse)
            {
                $("#updatable-entries").load("?partial-entries");
                $("#updatable-labels").load("?partial-labels");
                $("#updatable-total").load("?partial-total");

                $("#clearFilterButton").css( 'display', 'none' );
            }
        }
    );
    var form = document.getElementById( "addEntryForm" )
    form.reset();
    toggleAddExpenseFrame();
}

toggleAddExpenseFrame = function()
{
    var addExpenseFrame = document.getElementById("addEntryArea");
    var toolbar = document.getElementById("toolbar");

    if( addExpenseFrame.style.display != 'block' )
    {
        addExpenseFrame.style.display = 'block';
        toolbar.style.display = 'none';
    }
    else
    {
        addExpenseFrame.style.display = 'none';
        toolbar.style.display = 'block';
    }
}

filterByLabel = function( labelText )
{
    $("#updatable-entries").load("?partial-entries&label=" + encodeURIComponent( labelText ) );
    $("#updatable-labels").load("?partial-labels&label=" + encodeURIComponent( labelText ) );
    $("#updatable-total").load("?partial-total&label=" + encodeURIComponent( labelText ) );

    $("#clearFilterButton").css( 'display', 'inline-block' );
}

clearFilter = function()
{
    $("#updatable-entries").load("?partial-entries");
    $("#updatable-labels").load("?partial-labels");
    $("#updatable-total").load("?partial-total");

    $("#clearFilterButton").css( 'display', 'none' );
}

