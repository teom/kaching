/*
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
 */

package com.appspot.kachingmanager.data;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;

import java.util.*;

public class PersistenceHandler
{
    private static PersistenceHandler ourInstance = new PersistenceHandler();

    public static PersistenceHandler getInstance()
    {
        return ourInstance;
    }

    private PersistenceHandler()
    {
        ObjectifyService.register( Entry.class );
        ObjectifyService.register( Label.class );
    }

    public void insertEntry( String userId,
                        Date date,
                        double amount,
                        String description,
                        Label label )
    {
        Objectify ofy = ObjectifyService.begin();

        Entry entry = new Entry( userId, date, amount, description,
                                 new Key<Label>( Label.class, label.getId() ) );
        ofy.put( label );
        ofy.put( entry );
    }
    
    public void removeEntry( long id )
    {
        Objectify ofy = ObjectifyService.begin();
        Entry e = ofy.get( Entry.class, id );
        Key< Label > lKey = e.getLabel();
        ofy.delete( Entry.class, id );

        Query< Entry > q = ofy.query( Entry.class )
                .filter( "label", lKey );

        if( q.list().isEmpty() )
            ofy.delete( lKey );
    }

    public List< Entry > getAllEntries( String userId )
    {
        Objectify ofy = ObjectifyService.begin();

        List< Entry > entries;
        Query< Entry > q = ofy.query( Entry.class )
                .filter( "userId", userId )
                .order( "-date" );
        entries = q.list();
        return entries;
    }
    
    public List< Entry > getEntriesForLabel( String userId, Label label )
    {
        Objectify ofy = ObjectifyService.begin();

        List< Entry > entries;
        Query< Entry > q = ofy.query( Entry.class )
                .filter( "userId", userId )
                .filter( "label", Key.create( Label.class, label.getId() ) )
                .order( "-date" );
        entries = q.list();
        return entries;
    }
    
    public Map< Key<Label>, Label > getAllLabels( String userId )
    {
        Objectify ofy = ObjectifyService.begin();

        Map< Key<Label>, Label > labels = new HashMap<Key<Label>, Label>();
        Query< Label > q = ofy.query( Label.class )
                .filter( "userId", userId );
        if( q.count() > 0 )
        {
            for( Label l : q )
            {
                Key<Label> k = Key.create( Label.class, l.getId() );
                labels.put( k, l );
            }
        }
        return labels;
    }

    public Map< Key<Label>, Label > getLabelsExcept( String userId, Label label )
    {
        Objectify ofy = ObjectifyService.begin();

        Map< Key<Label>, Label > labels = new HashMap<Key<Label>, Label>();
        Query< Label > q = ofy.query( Label.class )
                .filter( "userId", userId )
                .filter( "id !=", label.getId() );
        if( q.count() > 0 )
        {
            for( Label l : q )
            {
                Key<Label> k = Key.create( Label.class, l.getId() );
                labels.put( k, l );
            }
        }
        return labels;
    }
}
