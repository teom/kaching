/*
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
 */

package com.appspot.kachingmanager.data;

import com.googlecode.objectify.Key;

import javax.persistence.Id;

import java.util.Date;

public class Entry
{
    @Id private Long id;

    private String userId;
    private Date date;
    private double amount;
    private String description;
    private Key<Label> label;
    
    private Entry()
    {
        userId = "";
        date = new Date();
        amount = 0;
        description = "";
    }
    
    public Entry( String userId, Date date, double amount,
                  String description, Key<Label> label )
    {
        this.userId = userId;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.label = label;
    }

    public Long getId()
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId( String userId )
    {
        this.userId = userId;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate( Date date )
    {
        this.date = date;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        this.amount = amount;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public Key<Label> getLabel()
    {
        return label;
    }

    public void setLabel( Key<Label> label )
    {
        this.label = label;
    }
}