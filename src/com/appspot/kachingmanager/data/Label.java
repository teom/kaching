/*
 *   Copyright 2012, Teo Mrnjavac <teo@kde.org>
 *
 *   This application is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This software is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this software. If not, see <http://www.gnu.org/licenses/>.
 */

package com.appspot.kachingmanager.data;

import com.googlecode.objectify.annotation.Entity;

import javax.persistence.Id;

@Entity
public class Label
{
    @Id private String id;
    private String userId;
    private String name;

    protected Label()
    {
        userId = "";
        name = "";
        id = userId + "." + name;
    }
    
    public Label( String userId, String name )
    {
        this.userId = userId;
        this.name = name;
        id = userId + "." + name;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }
}