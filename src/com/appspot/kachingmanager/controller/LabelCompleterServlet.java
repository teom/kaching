/*
*   Copyright 2012, Teo Mrnjavac <teo@kde.org>
*
*   This application is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This software is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this software. If not, see <http://www.gnu.org/licenses/>.
*/

package com.appspot.kachingmanager.controller;

import com.appspot.kachingmanager.data.Label;
import com.appspot.kachingmanager.data.PersistenceHandler;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

public class LabelCompleterServlet extends HttpServlet
{
    protected void doGet( HttpServletRequest request,
                          HttpServletResponse response )
            throws ServletException, IOException
    {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if( user == null )
        {
            response.sendRedirect( "/" );
        }
        
        PrintWriter out = response.getWriter();
        try
        {
            Collection<Label> labels = PersistenceHandler
                    .getInstance()
                    .getAllLabels( user.getUserId() )
                    .values();

            String q = request.getParameter("term");

            ArrayList< String > labelList = new ArrayList<String>();
            
            for( Label l : labels )
            {
                if( l.getName().toLowerCase().startsWith( q ) )
                    labelList.add( l.getName() );
            }
            Gson gson = new Gson();

            String json = gson.toJson( labelList );
            out.print( json );
        }
        finally
        {
            out.close();
        }
    }
}
