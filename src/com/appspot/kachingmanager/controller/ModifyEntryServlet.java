/*
*   Copyright 2012, Teo Mrnjavac <teo@kde.org>
*
*   This application is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This software is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this software. If not, see <http://www.gnu.org/licenses/>.
*/

package com.appspot.kachingmanager.controller;

import com.appspot.kachingmanager.data.Label;
import com.appspot.kachingmanager.data.PersistenceHandler;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ModifyEntryServlet extends HttpServlet
{
    protected void doPost( HttpServletRequest request,
                           HttpServletResponse response )
            throws ServletException, IOException
    {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if( user == null )
        {
            response.sendRedirect( "/" );
        }

        String labelParam = request.getParameter( "label" );
        String amountParam = request.getParameter( "amount" );
        String descriptionParam = request.getParameter( "description" );
        String dateParam = request.getParameter( "date" );
        String idParam = request.getParameter( "id" );

        if( idParam != null && !idParam.isEmpty() && labelParam != null && !labelParam.isEmpty()
                && amountParam != null && !amountParam.isEmpty() && descriptionParam != null )
        {
            PersistenceHandler ph = PersistenceHandler.getInstance();
            double amount = Double.valueOf( amountParam );
            long id = Long.valueOf( idParam );
            Label label = new Label( user.getUserId(), labelParam );

            Date date = null;
            try
            {
                date = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                        .parse( dateParam );
            }
            catch( ParseException e )
            {
                date = new Date();
            }
            ph.removeEntry( id );
            ph.insertEntry( user.getUserId(), date , amount, descriptionParam, label );
        }
    }

    protected void doGet( HttpServletRequest request,
                          HttpServletResponse response )
            throws ServletException, IOException
    {
        response.sendError( HttpServletResponse.SC_BAD_REQUEST, "I don't respond to GET." );
    }
}
