/*
*   Copyright 2012, Teo Mrnjavac <teo@kde.org>
*
*   IntelliJ IDEA is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   IntelliJ IDEA is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with IntelliJ IDEA. If not, see <http://www.gnu.org/licenses/>.
*/
package com.appspot.kachingmanager.controller;

import com.appspot.kachingmanager.data.Entry;
import com.appspot.kachingmanager.data.Label;
import com.appspot.kachingmanager.data.PersistenceHandler;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MainServlet extends HttpServlet
{
    protected void doPost( HttpServletRequest request,
                           HttpServletResponse response )
            throws ServletException, IOException
    {
    }

    protected void doGet( HttpServletRequest request,
                          HttpServletResponse response )
            throws ServletException, IOException
    {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if( user == null )
        {
            response.sendRedirect( "/" );
        }
        
        request.setAttribute( "user", user );
        request.setAttribute( "logoutUrl", userService.createLogoutURL( "/" ) );
        
        PersistenceHandler ph = PersistenceHandler.getInstance();

        String labelFilter = request.getParameter( "label" );
        List< Entry > entries = null;
        Map< Key< Label >, Label> labels = null;

        if( labelFilter == null )
        {
            entries = ph.getAllEntries( user.getUserId() );
            labels = ph.getAllLabels( user.getUserId() );
        }
        else
        {
            Label myLabel = new Label( user.getUserId(), labelFilter );
            entries = ph.getEntriesForLabel( user.getUserId(), myLabel );
            labels = ph.getLabelsExcept( user.getUserId(), myLabel );
            request.setAttribute( "activeLabel", labelFilter );
        }

        double total = 0;
        for( Entry e : entries )
        {
            total += e.getAmount();
        }

        request.setAttribute( "entries", entries );
        request.setAttribute( "labels", labels );
        request.setAttribute( "total", total );

        String dispatcherUrl = null;
        if( request.getParameter( "partial-entries" ) != null )
            dispatcherUrl = "/expense-rows.jsp";
        else if( request.getParameter( "partial-labels" ) != null )
            dispatcherUrl = "/labels-rows.jsp";
        else if( request.getParameter( "partial-total" ) != null )
            dispatcherUrl = "/total.jsp";
        else
            dispatcherUrl = "/main.jsp";
        
        ServletContext sc = getServletContext();
        RequestDispatcher dispatcher = sc.getRequestDispatcher( dispatcherUrl );
        dispatcher.forward( request, response );
    }
}
